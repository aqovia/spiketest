﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spike.Controllers;

namespace SpikeTesting
{
    [TestClass]
    public class SpikeTester
    {
        [TestMethod]
        public void KendoTest()
        {
            var kendo = new KendoController();
            Assert.AreEqual("Kendo", kendo.Name);
        }
    }
}
