﻿using System.Web;
using System.Web.Optimization;

namespace Spike
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                //"~/Scripts/kendo/kendo.all.min.js", //also includes dataviz which disables automatic server-side processing through ToDataSourceResult
                        "~/Scripts/kendo/kendo.web.min.js",
                        "~/Scripts/kendo/kendo.aspnetmvc.min.js",
                        "~/Scripts/kendo.modernizr.custom.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo-viz").Include(
                "~/Scripts/kendo/kendo.all.min.js",
                "~/Scripts/kendo/kendo.aspnetmvc.min.js")); //kendo.dataviz.min.js
            
            bundles.Add(new ScriptBundle("~/bundles/jqwidgets").Include("~/Scripts/jqwidgets/jqx-all.js"));

            bundles.Add(new ScriptBundle("~/bundles/extjs").Include("~/Scripts/extjs/bootstrap.js"));  

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new StyleBundle("~/Content/kendo").Include(
                        "~/Content/kendo/kendo.common.min.css",
                        "~/Content/kendo/kendo.rtl.min.css",
                        "~/Content/kendo/kendo.default.min.css"));

            bundles.Add(new StyleBundle("~/Content/kendo-viz").Include(
            "~/Content/kendo/kendo.common.min.css",
            "~/Content/kendo/kendo.rtl.min.css",
            "~/Content/kendo/kendo.default.min.css",
            "~/Content/kendo/kendo.dataviz.min.css"));

            

            bundles.Add(new StyleBundle("~/Content/jqwidgets").Include("~/Scripts/jqwidgets/styles/jqx.base.css"));

            bundles.Add(new StyleBundle("~/Content/extjs").Include("~/Scripts/extjs/resources/css/ext-all.css"));
        }
    }
}