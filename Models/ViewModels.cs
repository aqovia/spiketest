﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spike.Models
{
    public class ViewModels
    {
        public class AllViewModel
        {
            public int CompanyId { get; set; }
            public string CompanyName { get; set; }
            public int CountryId { get; set; }
            public string CountryName { get; set; }
            public string SiteName { get; set; }
            public string RoomName { get; set; }
            public string RowName { get; set; }
            public string CabinetName { get; set; }
            public int PanelNumber { get; set; }
            public int PortNumber { get; set; }
            public int PortId { get; set; }
        }

        public class AllPageableViewModel
        {
            public int totalCount;
            public IQueryable<AllViewModel> data;
        }

        public class PortsByCountriesViewModel
        {
            public string CountryName;
            public int Ports;
        }
    }
}