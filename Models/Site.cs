//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Spike.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Site
    {
        public Site()
        {
            this.Rooms = new HashSet<Room>();
        }
    
        public int SiteId { get; set; }
        public int CountryId { get; set; }
        public string SiteName { get; set; }
    
        public virtual Country Country { get; set; }
        public virtual ICollection<Room> Rooms { get; set; }
    }
}
