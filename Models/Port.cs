//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Spike.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Port
    {
        public Port()
        {
            this.CrossConnects = new HashSet<CrossConnect>();
            this.CrossConnects1 = new HashSet<CrossConnect>();
        }
    
        public int PortId { get; set; }
        public int PanelId { get; set; }
        public int PortNumber { get; set; }
    
        public virtual ICollection<CrossConnect> CrossConnects { get; set; }
        public virtual ICollection<CrossConnect> CrossConnects1 { get; set; }
        public virtual Panel Panel { get; set; }
    }
}
