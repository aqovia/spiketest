﻿

namespace Spike.Models
{
    public class Response
    {
        public Response(object data, int count)
        {
            Data = data;
            Count = count;
        }

        public object Data { get; set; }
        public int Count { get; set; }
    }
}