﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System.Web.Mvc;

namespace Spike.Models
{
    public static class SpikeProvider
    {
        const string SpikeContextKey = "SpikeDbContext";

        public static SpikeDB DB
        {
            get
            {
                if (HttpContext.Current.Items[SpikeContextKey] == null)
                    HttpContext.Current.Items[SpikeContextKey] = new SpikeDB();
                return (SpikeDB)HttpContext.Current.Items[SpikeContextKey];
            }
        }

        public static IQueryable GetCompanies()
        {
            //DB.Companies.Load();
            //json requires an anonymous (or poco) type returned
            return (from company in DB.Companies 
                    //orderby company.CompanyName
                    select new  {
                        CompanyId = company.CompanyId,
                        CompanyName = company.CompanyName
                    }
                    ).AsQueryable();
        }

        public static IEnumerable<Company> GetCompaniesList()
        {
            var companies = DB.Companies.ToList();
            companies.Insert(0, new Company{ CompanyId = 0, CompanyName = String.Empty });
            return companies;
        }

        public static IQueryable GetCountries(int? companyId)
        {
            return (from country in DB.Countries
                    where companyId == null || country.CompanyId == companyId
                    orderby country.CountryName
                    select new
                    {
                        CountryId = country.CountryId,
                        CountryName = country.CountryName,
                        CompanyId = country.CompanyId
                    }
                    ).AsQueryable();
        }

        public static IEnumerable<Country> GetCountriesList(int? companyId)
        {
            var countries = DB.Countries.Where(w => companyId == null || w.CompanyId == companyId).ToList();
            countries.Insert(0, new Country { CountryId = 0, CountryName = String.Empty, CompanyId = 0});
            return countries;
        }

        public static IQueryable GetPorts()
        {
            return (from port in DB.Ports select port).AsQueryable();
        }

        public static IQueryable GetSites(int companyId, int countryId)
        {
            return (from site in DB.Sites
                    join country in DB.Countries on site.CountryId equals country.CountryId
                    join company in DB.Companies on country.CompanyId equals company.CompanyId
                    where company.CompanyId == companyId && country.CountryId == countryId
                    select new { SiteId = site.SiteId, SiteName = site.SiteName }
                    ).AsQueryable();
        }

        public static IQueryable<ViewModels.AllViewModel> GetAll()
        {
            // convert to view model to avoid json serialization problems due to circular references
            return (from port in DB.Ports                    
                    join panel in DB.Panels on port.PanelId equals panel.PanelId                    
                    join cabinet in DB.Cabinets on panel.CabinetId equals cabinet.CabinetId
                    join row in DB.Rows on cabinet.RowId equals row.RowId
                    join room in DB.Rooms on row.RoomId equals room.RoomId
                    join site in DB.Sites on room.SiteId equals site.SiteId
                    join country in DB.Countries on site.CountryId equals country.CountryId
                    join company in DB.Companies on country.CompanyId equals company.CompanyId
                    select new ViewModels.AllViewModel
                    {
                        CompanyId = company.CompanyId,
                        CompanyName = company.CompanyName, 
                        CountryName = country.CountryName, 
                        SiteName = site.SiteName, 
                        RoomName = room.RoomName, 
                        RowName = row.RowName, 
                        CabinetName = cabinet.CabinetName,
                        PanelNumber = panel.PanelNumber, 
                        PortNumber = port.PortNumber, 
                        PortId = port.PortId
                    }
                    ).AsQueryable();
        }

        public static IQueryable<ViewModels.AllViewModel> GetAll(int? companyId)
        {
            return (from port in GetAll() 
                   where companyId == null || port.CompanyId == companyId
                   select port
                   ).AsQueryable();
        }

        public static ViewModels.AllPageableViewModel GetAll(int? page, int? limit, int? companyId)
        {
            var pageSize = limit ?? 20;
            var pageNum = page ?? 1;
            IQueryable<ViewModels.AllViewModel> query = GetAll(companyId);

            var allView = new ViewModels.AllPageableViewModel();
            allView.totalCount = query.Count();
            var skipRecords = ((pageNum == 0) ? pageNum : (pageNum - 1)) * pageSize;
            if (skipRecords > allView.totalCount)
            {
                skipRecords = 0;
            }
            allView.data = query.OrderBy(o => o.PortId).Skip(skipRecords).Take(pageSize);

            return allView;
        }

        public static IQueryable<ViewModels.PortsByCountriesViewModel> GetPortsByCountries()
        {
            return (from port in DB.Ports
                    join panel in DB.Panels on port.PanelId equals panel.PanelId
                    join cabinet in DB.Cabinets on panel.CabinetId equals cabinet.CabinetId
                    join row in DB.Rows on cabinet.RowId equals row.RowId
                    join room in DB.Rooms on row.RoomId equals room.RoomId
                    join site in DB.Sites on room.SiteId equals site.SiteId
                    join country in DB.Countries on site.CountryId equals country.CountryId                    
                    group country.CountryName by country.CountryName into g
                    select new ViewModels.PortsByCountriesViewModel
                    {
                        CountryName = g.Key,
                        Ports = g.Count()
                    }
                    ).AsQueryable();
        }

        public static IEnumerable<Country> GetPortsByCountriesList()
        {
            return DB.Countries.ToList();
        }        
    }
}