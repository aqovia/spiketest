﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spike.Models;
using System.Data.Entity;

namespace Spike.Controllers
{
    public class ExtJSController : BaseController
    {
        //
        // GET: /ExtJS/

        public ActionResult Grid()
        {
            return View();
        }

        public ActionResult GridMasterDetail()
        {
            return View();
        }

        public ActionResult CascadingCombos()
        {
            return View();
        }

        public ActionResult Chart()
        {
            return View();
        }

        public override ActionResult All_Read(int? page, int? limit, int? companyId)
        {
            return base.All_Read(page, limit, companyId);
        }
    }
}
