﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spike.Models;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxClasses.Internal;
using DevExpress.Web.Mvc;
using System.Web.UI.WebControls;

namespace Spike.Controllers
{
    public class DevExpressController : BaseController
    {
        //
        // GET: /DevExpress/

        public ActionResult Grid()
        {
            return View();
        }

        public ActionResult GridPartial()
        {
            return PartialView();
        }

        public ActionResult CascadingCombos()
        {
            return View(new ViewModels.AllViewModel());
        }

        public ActionResult CompanyPartial()
        {
            return PartialView(new ViewModels.AllViewModel());
        }

        public ActionResult CountryPartial(int companyId)
        {
            ViewData["CompanyId"] = companyId;
            return PartialView(new ViewModels.AllViewModel { CompanyId = companyId });
        }

        public ActionResult Chart()
        {
            return View(SpikeProvider.GetPortsByCountriesList());
        }


        public ActionResult Export()
        {
            if (!string.IsNullOrEmpty(Request["PDF"]))
            {
                return GridViewExtension.ExportToPdf(ExportGridSettings(), SpikeProvider.GetAll().ToList());
            }

            if (!string.IsNullOrEmpty(Request["XLSX"]))
            {
                return GridViewExtension.ExportToXlsx(ExportGridSettings(), SpikeProvider.GetAll().ToList());
            }

            return RedirectToAction("Grid");
        }

        public GridViewSettings ExportGridSettings() 
        {
            var settings = new GridViewSettings();

            settings.Name = "Ports";
            settings.CallbackRouteValues = new { Controller = "Export", Action = "DevExpress" };
            settings.Width = Unit.Percentage(100);

            settings.Columns.Add("CompanyName", "Company");
            settings.Columns.Add("CountryName", "Country");
            settings.Columns.Add("SiteName", "Site");
            settings.Columns.Add("RoomName", "Room");
            settings.Columns.Add("RowName", "Row");
            settings.Columns.Add("CabinetName", "Cabinet");
            settings.Columns.Add("PanelNumber", "Panel");
            settings.Columns.Add("PortNumber", "Port");

            return settings;
        }

        public ActionResult GridMasterDetail()
        {
            return View();
        }

        public ActionResult GridMasterPartial()
        {
            return PartialView();
        }

        public ActionResult GridDetailPartial(int companyId)
        {
            ViewData["CompanyId"] = companyId;
            return PartialView();
        }

    }
}
