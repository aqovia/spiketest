﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spike.Models;

namespace Spike.Controllers
{
    public class JQWidgetsController : BaseController
    {
        //
        // GET: /JQWidgets/

        public ActionResult Grid()
        {
            return View();
        }

        public ActionResult GridMasterDetail()
        {
            return View();
        }

        public ActionResult CascadingCombos()
        {
            return View();
        }

        public ActionResult Chart()
        {
            return View();
        }        
    }
}
