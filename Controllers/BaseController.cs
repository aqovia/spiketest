﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spike.Models;

namespace Spike.Controllers
{
    public class BaseController : Controller
    {
        public virtual string Name { get; set; }

        //
        // GET: /Base/

        public ActionResult Companies_Read()
        {
            return Json(SpikeProvider.GetCompanies(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Countries_Read(int? companyId)
        {
            return Json(SpikeProvider.GetCountries(companyId), JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult All_Read(int? pagenum, int? pagesize, int? companyId)
        {
            var json = new JsonResult();
            // for larger resultsets
            json.MaxJsonLength = int.MaxValue;
            json = Json(SpikeProvider.GetAll(pagenum, pagesize, companyId), JsonRequestBehavior.AllowGet);
            return json;
        }

        public ActionResult PortsByCountries()
        {
            return Json(SpikeProvider.GetPortsByCountries(), JsonRequestBehavior.AllowGet);
        }

    }
}
