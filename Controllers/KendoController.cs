﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Spike.Models;
using System.Web.Script.Serialization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.HPSF;

namespace Spike.Controllers
{
    public class KendoController : BaseController
    {
        //
        // GET: /Kendo/

        public override string Name
        {
            get { return "Kendo"; }
        }

        public ActionResult Grid()
        {
            return View();
        }

        public ActionResult GridMasterDetail()
        {
            return View();
        }

        public ActionResult CascadingCombos()
        {
            return View();
        }

        public ActionResult Chart()
        {
            return View();
        }

        public ActionResult RestKendo()
        {
            return View();
        }
        public override ActionResult All_Read(int? pagenum, int? pagesize, int? companyId)
        {
            var pageNum = pagenum ?? -1;
            if (pageNum > -1)
            {
                pageNum++;
            }
            else
            {
                pageNum = 1;
            }

            return base.All_Read(pageNum, pagesize, companyId);
        }

        public ActionResult Kendo_Companies_Read_Raw()
        {
            // combo expects json data in root
            return Json(SpikeProvider.GetCompanies(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Kendo_Companies_Read([DataSourceRequest] DataSourceRequest request)
        {
            return Json(SpikeProvider.GetCompanies().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Kendo_CountriesForCompany_Read(int? companyId)
        {
            return Json(SpikeProvider.GetCountries(companyId), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Kendo_All_Read([DataSourceRequest] DataSourceRequest request)
        {
            var json = new JsonResult();
            // for larger resultsets
            json.MaxJsonLength = int.MaxValue;
            json = Json(SpikeProvider.GetAll().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            return json;
        }
        
        public ActionResult Kendo_AllForCompany_Read(int companyId, [DataSourceRequest] DataSourceRequest request)
        {
            var json = new JsonResult();
            // for larger resultsets
            json.MaxJsonLength = int.MaxValue;
            json = Json(SpikeProvider.GetAll(companyId).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            return json;
        }

        public ActionResult Kendo_SitesForCountry_Read(int companyId, int countryId)
        {
            var json = new JsonResult();
            // for larger resultsets
            json.MaxJsonLength = int.MaxValue;
            json = Json(SpikeProvider.GetSites(companyId, countryId), JsonRequestBehavior.AllowGet);
            return json;
        }

        public ActionResult ExportPDF([DataSourceRequest] DataSourceRequest request)
        {
            var ports = SpikeProvider.GetAll().ToDataSourceResult(request).Data;

            var document = new Document(PageSize.A4, 10, 10, 10, 10);
            document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            var output = new MemoryStream();
            PdfWriter.GetInstance(document, output);

            document.Open();

            var numOfColumns = 8;
            var dataTable = new PdfPTable(numOfColumns);
            dataTable.WidthPercentage = 100;
            dataTable.DefaultCell.Padding = 3;

            dataTable.DefaultCell.BorderWidth = 2;
            dataTable.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;

            dataTable.AddCell("Company");
            dataTable.AddCell("Country");
            dataTable.AddCell("Site");
            dataTable.AddCell("Room");
            dataTable.AddCell("Row");
            dataTable.AddCell("Cabinet");
            dataTable.AddCell("Panel");
            dataTable.AddCell("Port");

            dataTable.HeaderRows = 1;
            dataTable.DefaultCell.BorderWidth = 1;

            foreach (ViewModels.AllViewModel port in ports)
            {
                dataTable.AddCell(port.CompanyName);
                dataTable.AddCell(port.CountryName);
                dataTable.AddCell(port.SiteName);
                dataTable.AddCell(port.RoomName);
                dataTable.AddCell(port.RowName);
                dataTable.AddCell(port.CabinetName);
                dataTable.AddCell(port.PanelNumber.ToString());
                dataTable.AddCell(port.PortNumber.ToString());
            }

            document.Add(dataTable);
            document.Close();

            return File(output.ToArray(), "application/pdf", "ports.pdf");
        }

        public ActionResult ExportXLSX([DataSourceRequest]DataSourceRequest request)
        {         
            var ports = SpikeProvider.GetAll().ToDataSourceResult(request).Data;

            var workbook = new HSSFWorkbook();

            var sheet = workbook.CreateSheet();

            const int unit = 256; // width expressed in 1/256 character width
            sheet.SetColumnWidth(0, 25 * unit);
            sheet.SetColumnWidth(1, 15 * unit);
            sheet.SetColumnWidth(2, 15 * unit);
            sheet.SetColumnWidth(3, 15 * unit);
            sheet.SetColumnWidth(4, 15 * unit);
            sheet.SetColumnWidth(5, 15 * unit);
            sheet.SetColumnWidth(6, 15 * unit);
            sheet.SetColumnWidth(7, 15 * unit);

            ICellStyle style = workbook.CreateCellStyle();
            style.Rotation = 5;
            style.FillForegroundColor = HSSFColor.LightYellow.Index;
            style.Alignment = HorizontalAlignment.Center;
            style.FillPattern = FillPattern.SolidForeground;
            sheet.SetDefaultColumnStyle(7, style);

            int rowNumber = 1;

            var headerRow = sheet.CreateRow(rowNumber++);

            headerRow.CreateCell(0).SetCellValue("Company");
            headerRow.CreateCell(1).SetCellValue("Country");
            headerRow.CreateCell(2).SetCellValue("Site");
            headerRow.CreateCell(3).SetCellValue("Room");
            headerRow.CreateCell(4).SetCellValue("Row");
            headerRow.CreateCell(5).SetCellValue("Cabinet");
            headerRow.CreateCell(6).SetCellValue("Panel");
            headerRow.CreateCell(7).SetCellValue("Port");

            sheet.CreateFreezePane(0, 1, 0, 1);            

            foreach (ViewModels.AllViewModel port in ports)
            {
                var row = sheet.CreateRow(rowNumber++);

                row.CreateCell(0).SetCellValue(port.CompanyName);
                row.CreateCell(1).SetCellValue(port.CountryName);
                row.CreateCell(2).SetCellValue(port.SiteName);
                row.CreateCell(3).SetCellValue(port.RoomName);
                row.CreateCell(4).SetCellValue(port.RowName);
                row.CreateCell(5).SetCellValue(port.CabinetName);
                row.CreateCell(6).SetCellValue(port.PanelNumber.ToString());
                row.CreateCell(7).SetCellValue(port.PortNumber.ToString());
            }

            MemoryStream output = new MemoryStream();
            workbook.Write(output);

            return File(output.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ports.xlsx");

        }
    }
}
