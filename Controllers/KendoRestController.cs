﻿using System.Web.Http;
using System.Web.Http.ModelBinding;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Spike.Models;
using DataSourceRequestModelBinder = Spike.Binders.DataSourceRequestModelBinder;

namespace Spike.Controllers
{
    public class KendoRestController : ApiController
    {
        
        public DataSourceResult Get([ModelBinder(typeof(DataSourceRequestModelBinder))] DataSourceRequest request)
        {
            return SpikeProvider.GetAll().ToDataSourceResult(request);
        }

    }
}
